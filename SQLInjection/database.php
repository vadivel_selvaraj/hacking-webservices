<?php

class Database
        {
            // Store the single instance of Database
            private static $m_pInstance;

      		private $m_connection; 	
            /* mysql hostname */
			private $hostname = 'localhost';

			/* mysql username */
			private $username = 'root';

			/* mysql password */	
			private $password = '';

            // Private constructor to limit object instantiation to within the class
            private function __construct() 
            {
                $this->m_connection = new mysqli($this->hostname, $this->username, $this->password, "network");
				if ($this->m_connection->connect_error) {
					
					die("Connection failed: " . $conn->connect_error);
				
				}else{

					// echo " &nbsp &nbsp";
// 					echo "Connection Successful!!";
// 					 echo "<br>" ; 
					// log that the connectio is successful.
				
				}	
            }

            // Getter method for creating/returning the single instance of this class
            public static function getInstance()
            {
                if (!self::$m_pInstance)
                {
                  self::$m_pInstance = new Database();
                }
                return self::$m_pInstance;
            }

            public function getconnection()
            {
            	return $this->m_connection;

            }

            public function query($query)
            {
               return $this->m_connection->query($query);
            }
            
             public function multi_query($query)
            {
               return $this->m_connection->multi_query($query);
            }

         }

?>
