### What is this repository for? ###

This repository is for our final project in Network Security course. Our project has 3 phases.

* Provide a Proof of Concept (POC) that we could gain access to critical data via Web services if they are not properly implemented. We are thinking of targeting an android taxi app and finding out all cabs that are available in a particular area. This could provide valuable insights to a competitor.
* SQL Injection via web service. This can be as harmful as bringing down the authentication system of the application when executing a 'delete * from user query.
* Denial of Service by hitting the web service periodically. This will render the application unresponsive for valid users and cause revenue loss.

### How do I get set up? ###

See README.md for individual phase folders.

### Who do I talk to? ###

* Vadivel @ vadivel.selvaraj@utdallas.edu
* Santosh Kumar Kompally @ 
* Raveena Singh
* Pavan Kesava Rao