/*
* Main.js
* - Encloses all the application logic including querying the backend for all search queries.
*/

var place, source, destination, sessionid;
var addresses = [{
    "AdminLevel2": "state capitol",
        "StreetName": "12th Street West",
        "PostCode": "78701",
        "AdminLevel1": "TX",
        "Country": "United States",
        "Locality1": "Austin",
        "GEOLocation": {
        "Latitude": 30.2746698,
            "Longitude": -97.7425392
    }
}, {
    "AdminLevel2": "sixth street",
        "StreetName": "12th Street West",
        "PostCode": "78703",
        "AdminLevel1": "TX",
        "Country": "United States",
        "Locality1": "Austin",
        "GEOLocation": {
        "Latitude": 30.2730245,
            "Longitude": -97.7601438
    }
}, {
    "AdminLevel2": "UT Austin",
        "StreetName": "12th Street West",
        "PostCode": "78703",
        "AdminLevel1": "TX",
        "Country": "United States",
        "Locality1": "Austin",
        "GEOLocation": {
        "Latitude": 30.2849231,
            "Longitude": -97.7362454
    }
}, {
    "AdminLevel2": "mount bonell",
        "StreetName": "12th Street West",
        "PostCode": "78731",
        "AdminLevel1": "TX",
        "Country": "United States",
        "Locality1": "Austin",
        "GEOLocation": {
        "Latitude": 30.3207662,
            "Longitude": -97.7755289
    }
}, {
    "AdminLevel2": "lake travis",
        "StreetName": "12th Street West",
        "PostCode": "78738",
        "AdminLevel1": "TX",
        "Country": "United States",
        "Locality1": "Austin",
        "GEOLocation": {
        "Latitude": 30.4639467,
            "Longitude": -98.2047277
    }
}, {
    "AdminLevel2": "lake Austin",
        "StreetName": "12th Street West",
        "PostCode": "78738",
        "AdminLevel1": "TX",
        "Country": "United States",
        "Locality1": "Austin",
        "GEOLocation": {
        "Latitude": 30.3436219,
            "Longitude": -97.922156
    }
}, {
    "AdminLevel2": "Austin Zoo",
        "StreetName": "12th Street West",
        "PostCode": "78736",
        "AdminLevel1": "TX",
        "Country": "United States",
        "Locality1": "Austin",
        "GEOLocation": {
        "Latitude": 30.2618346,
            "Longitude": -97.939246
    }
}, {
    "AdminLevel2": "barton springs pool",
        "StreetName": "12th Street West",
        "PostCode": "78746",
        "AdminLevel1": "TX",
        "Country": "United States",
        "Locality1": "Austin",
        "GEOLocation": {
        "Latitude": 30.2636401,
            "Longitude": -97.7728872
    }
}, {
    "AdminLevel2": "zilker park",
        "StreetName": "12th Street West",
        "PostCode": "78746",
        "AdminLevel1": "TX",
        "Country": "United States",
        "Locality1": "Austin",
        "GEOLocation": {
        "Latitude": 30.266967,
            "Longitude": -97.775048
    }
}, {
    "AdminLevel2": "hamilton pool",
        "StreetName": "12th Street West",
        "PostCode": "78620",
        "AdminLevel1": "TX",
        "Country": "United States",
        "Locality1": "Austin",
        "GEOLocation": {
        "Latitude": 30.3422856,
            "Longitude": -98.1292593
    }
}, {
    "AdminLevel2": "lady bird lake",
        "StreetName": "12th Street West",
        "PostCode": "78741",
        "AdminLevel1": "TX",
        "Country": "United States",
        "Locality1": "Austin",
        "GEOLocation": {
        "Latitude": 30.2690253,
            "Longitude": -97.7858275
    }
}, {
    "AdminLevel2": "bullock texas state history museum",
        "StreetName": "12th Street West",
        "PostCode": "78701",
        "AdminLevel1": "TX",
        "Country": "United States",
        "Locality1": "Austin",
        "GEOLocation": {
        "Latitude": 30.2803694,
            "Longitude": -97.7413623
    }
}, {
    "AdminLevel2": "mexic-arte museum",
        "StreetName": "12th Street West",
        "PostCode": "78701",
        "AdminLevel1": "TX",
        "Country": "United States",
        "Locality1": "Austin",
        "GEOLocation": {
        "Latitude": 30.2668548,
            "Longitude": -97.74497
    }
}, {
    "AdminLevel2": "hippie hollow park",
        "StreetName": "12th Street West",
        "PostCode": "78732",
        "AdminLevel1": "TX",
        "Country": "United States",
        "Locality1": "Austin",
        "GEOLocation": {
        "Latitude": 30.41316,
            "Longitude": -97.8862543
    }
}, {
    "AdminLevel2": "the paramount theatre",
        "StreetName": "12th Street West",
        "PostCode": "78701",
        "AdminLevel1": "TX",
        "Country": "United States",
        "Locality1": "Austin",
        "GEOLocation": {
        "Latitude": 30.2694556,
            "Longitude": -97.7443126
    }
}, {
    "AdminLevel2": "the driskill",
        "StreetName": "12th Street West",
        "PostCode": "78701",
        "AdminLevel1": "TX",
        "Country": "United States",
        "Locality1": "Austin",
        "GEOLocation": {
        "Latitude": 30.2681666,
            "Longitude": -97.7438887
    }
}, {
    "AdminLevel2": "city bridge segway tour",
    "StreetName": "12th Street West",
    "PostCode": "78701",
    "AdminLevel1": "TX",
    "Country": "United States",
    "Locality1": "Austin",
    "GEOLocation": {
        "Latitude": 30.2681895,
        "Longitude": -97.7504548

    }
}, {
        "AdminLevel2": "pease park",
            "StreetName": "12th Street West",
            "PostCode": "78705",
            "AdminLevel1": "TX",
            "Country": "United States",
            "Locality1": "Austin",
            "GEOLocation": {
            "Latitude": 30.2844604,
                "Longitude": -97.7562618

        }
    }
];

var gpsMap = {
    "state capitol": {
        "Latitude": 30.2746698,
        "Longitude": -97.7425392,
        "Address": addresses[0]
    },
    "sixth street": {
        "Latitude": 30.2730245,
        "Longitude": -97.7601438,
        "Address": addresses[1]
    },
    "UT Austin": {
        "Latitude": 30.2849231,
        "Longitude": -97.7362454,
        "Address": addresses[2]
    },
    "mount bonell": {
        "Latitude": 30.3207662,
        "Longitude": -97.7755289,
        "Address": addresses[3]
    },
    "lake travis": {
        "Latitude": 30.4639467,
        "Longitude": -98.2047277,
        "Address": addresses[4]
    },
    "lake Austin": {
        "Latitude": 30.3436219,
        "Longitude": -97.922156,
        "Address": addresses[5]
    },
    "Austin Zoo": {
        "Latitude": 30.2618346,
        "Longitude": -97.939246,
        "Address": addresses[6]
    },
    "barton springs pool": {
        "Latitude": 30.2636401,
        "Longitude": -97.7728872,
        "Address": addresses[7]
    },
    "zilker park": {
        "Latitude": 30.266967,
        "Longitude": -97.775048,
        "Address": addresses[8]
    },
    "hamilton pool": {
        "Latitude": 30.3422856,
        "Longitude": -98.1292593,
        "Address": addresses[9]
    },
    "lady bird lake": {
        "Latitude": 30.2690253,
        "Longitude": -97.7858275,
        "Address": addresses[10]
    },
    "bullock texas state history museum": {
        "Latitude": 30.2803694,
        "Longitude": -97.7413623,
        "Address": addresses[11]
    },
    "mexic-arte museum": {
        "Latitude": 30.2668548,
        "Longitude": -97.74497,
        "Address": addresses[12]
    },
    "hippie hollow park": {
        "Latitude": 30.41316,
        "Longitude": -97.8862543,
        "Address": addresses[13]
    },
    "the paramount theatre": {
        "Latitude": 30.2694556,
        "Longitude": -97.7443126,
        "Address": addresses[14]
    },
    "the driskill": {
        "Latitude": 30.2681666,
        "Longitude": -97.7438887,
        "Address": addresses[15]
    },
    "city bridge segway tour": {
        "Latitude": 30.2681895,
        "Longitude": -97.7504548,
        "Address": addresses[16]

    },
    "pease park": {
        "Latitude": 30.2844604,
        "Longitude": -97.7562618,
        "Address": addresses[17]
    }
};

$(document).ready(function() {
    // Set the selected text as the drop down's text.
    $("#place .dropdown-menu li a").click(function(){
        var selectedText = $(this).text();
        place = selectedText;
        $(this).parents('.btn-group').find('.dropdown-toggle').html(selectedText + '<span class="caret"></span>');
    });

    $("#source .dropdown-menu li a").click(function(){
        var selectedText = $(this).text();
        source = selectedText;
        $(this).parents('.btn-group').find('.dropdown-toggle').html(selectedText + '<span class="caret"></span>');
    });

    $("#destination .dropdown-menu li a").click(function(){
        var selectedText = $(this).text();
        destination = selectedText;
        $(this).parents('.btn-group').find('.dropdown-toggle').html(selectedText + '<span class="caret"></span>');
    });

    // On submit, call the backend to find the number of cabs.
    $('#submit1').click(function () {
        sessionid = $('#session-id').val();
        if (sessionid == null || sessionid == '') {
            alert("Please enter a session id");
            return;
        }
        if (place != null)
            getNumberOfCabs();
        else {
            alert('Please select a place');
        }
    });

    // On submit, call the backend to get the fares.
    $('#submit2').click(function () {
        sessionid = $('#session-id').val();
        if (sessionid == null || sessionid == '') {
            alert("Please enter a session id");
            return;
        }
        if (source != null && destination != null) {
            if (source == destination) {
                alert("Please make sure source and destination are different");
                return;
            }

            getFareEstimate();
        }
        else {
            alert('Please select a source and a destination');
        }
    });
});

function getNumberOfCabs() {
    $('#result1').html("");
    if (!gpsMap.hasOwnProperty(place)) {
        alert("gpsMap doesn't have '" + place + "'");
        return;
    }

    $.get("/getNumberOfCabs", {
        sessionid: sessionid,
        lat: gpsMap[place]['Longitude'],
        long: gpsMap[place]['Latitude']
    }).done(function (data) {
        if (data.hasOwnProperty("numberOfCabs")) {
            $('#result1').html("<div class='alert alert-success'>Number of cabs serving now: " +  data.numberOfCabs + "</div>");
        }
    });
}

function getFareEstimate() {
    $('#result2').html("");
    if (!gpsMap.hasOwnProperty(source)) {
        alert("gpsMap doesn't have '" + source + "'");
        return;
    }

    if (!gpsMap.hasOwnProperty(destination)) {
        alert("gpsMap doesn't have '" + destination + "'");
        return;
    }

    $.get("/getFareEstimate", {
        sessionid: sessionid,
        sourceaddress: gpsMap[source]['Address'],
        destinationaddress: gpsMap[destination]['Address']
    }).done( function (data) {
        if (data.hasOwnProperty("fareIncludingTolls") && data.hasOwnProperty("fareExcludingTolls")) {
            $('#result2').html("<div class='alert alert-success'>Estimated fare, with tolls: '" + data.fareIncludingTolls + "', without tolls: " + data.fareExcludingTolls + " </div>");
        }
    });
}