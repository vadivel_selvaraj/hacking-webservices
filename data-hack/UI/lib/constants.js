
module.exports =  Object.freeze({
    // Bing Credentials
    BING_ACCOUNT_KEY : "25f278c41e38478e9d5e882e3311bf35",

    // Google credentials
    GOOGLE_DEVELOPER_API_KEY : 'AIzaSyAqPsjne9l1kVOb6hmPS703soiiLTMbvlc',
    GOOGLE_SEARCH_ENGINE_ID : '002928185242721840036%3Alhgf_boamcy',

    SOLR_HOST : '104.154.202.79:8983',
    WEBPAGE_CONTENT_SIZE_IN_CHARS : 200,

    HITS_JAR_PATH: '../RelevanceModel/Hits',
    HITS_JAR_FILE_NAME: 'Hits.jar',

    QUERY_EXPANSION_JAR_PATH: '../QueryExpansion',
    QUERY_EXPANSION_JAR_FILE_NAME: 'QueryExpansion.jar',
    ASSOCIATION_CLUSTERING_METHOD_ID: 0,
    METRIC_CLUSTERING_METHOD_ID: 1,
    SCALAR_CLUSTERING_METHOD_ID: 2,

    SEARCH_RESULT_SIZE: 20

});