var fs = require('fs');

var fileExistsFn = function(filePath) {
	return fs.existsSync(filePath);
}

var limitText = function(text, limit){
	if (text == null || text.length == 0)
		return;
	if (text.length < limit)
		return text;

	for (var i = limit; i > 0; i--) {
		if(text.charAt(i) === ' ' && (text.charAt(i-1) != ','||text.charAt(i-1) != '.'||text.charAt(i-1) != ';')) {
			return text.substring(0, i) + '...';
		}
	}
}


module.exports = {
	fileExists: fileExistsFn,
	limitText: limitText
}