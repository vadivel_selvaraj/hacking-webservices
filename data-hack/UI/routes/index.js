const express = require('express');
const router = express.Router();

const requestLib = require('request');
var async = require('async');

const constants = require('../lib/constants');
const helper = require('../lib/utilities');

const util = require('util');
const assert = require('assert');


/* GET home page. */
router.get('/', function(request, response) {
	response.render('index', {
		title: 'Data Hack'
	});
});

var getNumberOfCabs = function(callback, sessionid, latitude, longitude) {
    //console.log("lat: " + latitude + " long: " + longitude);
    requestLib.post({
        url: "http://iphone.houstonyellowcab.com:8080/ServerBooker-Austin/BA/GHTG/api/CustomerApp/GetNearbyVehicles",
        form: {
            "Header": {
                "LoginSessionToken": "f0f1c50bbb514efd8782b30cf5432117",
                "IsValidGPS":true,
                "GEOStatus": {
                    "Latitude": 32.98696414521883,
                    "Longitude": -96.74765831719394,
                    "Direction": -1,
                    "Speed": -1
                },
                "DeviceTimeUTC": "2017-04-30T18:23:40Z",
                "DeviceID":"7B69C4CA-9D7F-4558-977F-63D3E19954F5"
            },
            "Data" : {
                "GEOLocation": {
                    "Latitude": latitude,
                    "Longitude": longitude
                }
            }
        },
        json: true
    }, function(error, response, body) {
        if (error) {
            console.log(error);
            callback(true);
            return;
        }

        console.log( "body: " + util.inspect(body, false, null) );
        //console.log( "response: " + util.inspect(response, false, null) );
        console.log("No. of vehicles: " + body["Data"]["Vehicles"].length );
        callback(false, body["Data"]["Vehicles"].length);
    });
};

var getFareEstimate = function(callback, sessionid, sourceAddress, destinationAddress) {
    requestLib.post({
        url: "http://iphone.houstonyellowcab.com:8080/ServerBooker-Austin/BA/GHTG/api/Booking/GetServiceEstimate",
        form: {
            "Header": {
                "LoginSessionToken": sessionid,
                "IsValidGPS":true,
                "GEOStatus": {
                    "Latitude": 32.98696414521883,
                    "Longitude": -96.74765831719394,
                    "Direction": -1,
                    "Speed": -1
                },
                "DeviceTimeUTC": "2017-04-30T18:23:40Z",
                "DeviceID":"7B69C4CA-9D7F-4558-977F-63D3E19954F5"
            },
            "Data" : {
                "Booking": {
                    "Pickup": {
                        "Address": sourceAddress,
                        "AddressType": 3
                    },
                    "Fleet": {
                        "Identifier": "1"
                    },
                    "RequestedDateUTC": "2017-04-29T02:31:24Z",
                    "PaymentMethod": 1,
                    "Destination": {
                        "Address": destinationAddress,
                        "AddressType": 3
                    },
                    "CarType": {
                        "Identifier": "-1",
                        "Name": "Any"
                    }
                },
                "RequestedDateType": 2
            }
        },
        json: true
    }, function(error, response, body) {
        if (error) {
            console.log(error);
            callback(true);
            return;
        }

        console.log("body: " + util.inspect(body, false, null));
        var fareData = body['Data'];
        var fareWithToll = fareData['IncTolls'] + fareData['CurrencySymbol'];
        var fareWithoutToll = fareData['ExcTolls'] + fareData['CurrencySymbol'];
        callback(false, {
            fareIncludingTolls: fareWithToll,
            fareExcludingTolls: fareWithoutToll
        });
    });
};

router.get('/getNumberOfCabs', function(request, response) {
    var sessionid = request.query.sessionid;
    var latitude = request.query.lat;
    var longitude = request.query.long;

    async.parallel([
        function (callback) {
            getNumberOfCabs(callback, sessionid, longitude, latitude);
        }
    ], function (error, result) {
        if (error) {
            console.log("error in async parallel call: " + error);
            response.status(500).send("Server Error");
            return;
        }

        // console.log("results: " + util.inspect(result, false, null));
        response.json({
            numberOfCabs: result[0]
        });
    });
});

router.get('/getFareEstimate', function(request, response) {
	var sessionid = request.query.sessionid;
    var sourceAddress = request.query.sourceaddress;
    var destinationAddress = request.query.destinationaddress;

    async.parallel([
        function (callback) {
            getFareEstimate(callback, sessionid, sourceAddress, destinationAddress);
        }
    ], function (error, result) {
        if (error) {
            console.log("error in async parallel call: " + error);
            response.status(500).send("Server Error");
            return;
        }
        console.log("results: " + util.inspect(result, false, null));
        response.json(result[0]);
    });

});

module.exports = router;