# README #

### How do I set up? ###

* Get the latest repo
```
git clone https://bitbucket.org/informartionretrievalproject/oscar
cd oscar/UI
```
 
* Install NPM dependencies
```
npm install
```

* Start the server
> Note: Before starting the server, make sure that bing/google developer keys are valid and solr is up and ready to serve.
```
npm start
```

### Tools used ###
* Mustache js for templating
* NodeJS for backend server
* async nodejs module for doing parallel http requests
* Jade View Engine for rendering html
* Bootstrap
* JQuery 

### Who do I talk to? ###

* Vadivel @ gct.vadivel@gmail.com
